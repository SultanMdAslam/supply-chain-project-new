package com.pondit.supplychainprocess.entity;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.repository.cdi.Eager;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Delivery {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long Id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column
    private String UserName;

    @Column
    private String productName;

    @Column
    private long quantity;

    public Delivery(User user, String userName, String productName, long quantity) {
        this.user = user;
        UserName = userName;
        this.productName = productName;
        this.quantity = quantity;
    }
}
