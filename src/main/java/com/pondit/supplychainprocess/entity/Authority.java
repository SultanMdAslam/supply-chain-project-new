package com.pondit.supplychainprocess.entity;

import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.List;

@Entity(name = "authority")
@Setter
public class Authority implements GrantedAuthority {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long roleId;

    @Column
    private String authority;

    public long getRoleId() {
        return roleId;
    }

    @Override
    public String getAuthority() {
        return authority;
    }

    public Authority(String authority){
        this.authority = authority;
    }

    public Authority() {
    }
}
