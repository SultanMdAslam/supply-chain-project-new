package com.pondit.supplychainprocess.entity;

import javax.persistence.*;

@Entity
public class DeliveryInCharge {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long Id;

    @Column
    private String name;
}
