package com.pondit.supplychainprocess.dto;

import com.pondit.supplychainprocess.entity.User;
import lombok.Data;

@Data
public class DealerDto {
    private long Id;
    private String name;
    private long nId;
    private long tId;
    private User user;
}
