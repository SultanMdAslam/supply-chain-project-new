package com.pondit.supplychainprocess.dto;

import com.pondit.supplychainprocess.entity.Authority;
import lombok.Data;

import java.util.List;

@Data
public class UserDto {
    private long userId;
    private String username;
    private String password;
    private Authority authority;
}
