package com.pondit.supplychainprocess.service;

import com.pondit.supplychainprocess.entity.Product;
import com.pondit.supplychainprocess.repository.ProductRepo;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

    ProductRepo productRepo;

    public Product getProductDetails(long id) {
        return productRepo.getOne(id);
    }
}
