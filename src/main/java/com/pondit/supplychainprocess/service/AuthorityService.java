package com.pondit.supplychainprocess.service;


import com.pondit.supplychainprocess.entity.Authority;
import com.pondit.supplychainprocess.repository.AuthorityRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorityService {
    @Autowired
    AuthorityRepo authorityRepo;


    public List<Authority> findAll() {
        return authorityRepo.findAll();
    }

    public Authority findAuthority() {
        Authority authority = authorityRepo.getOne(2L);
        return authority;
    }

    public Authority getAuthority(long authorityId) {
        return authorityRepo.getOne(authorityId);
    }
}
