package com.pondit.supplychainprocess;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SupplyChainProcessApplication {

    public static void main(String[] args) {
        SpringApplication.run(SupplyChainProcessApplication.class, args);
    }

}
