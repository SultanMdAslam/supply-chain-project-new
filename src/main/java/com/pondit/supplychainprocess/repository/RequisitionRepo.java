package com.pondit.supplychainprocess.repository;

import com.pondit.supplychainprocess.entity.Dealer;
import com.pondit.supplychainprocess.entity.Requisition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RequisitionRepo extends JpaRepository<Requisition, Long> {

    List<Requisition> getAllByActiveFalseAndEnableFalse();

    List<Requisition> getAllByActiveTrueAndEnableFalse();

    List<Requisition> getAllByActiveTrueAndEnableTrue();

    List<Requisition> getAllByActiveFalseAndEnableTrue();

    List<Requisition> findByDealerAndFullPaymentFalse(Dealer dealer);


}
