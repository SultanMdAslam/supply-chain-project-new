package com.pondit.supplychainprocess.repository;

import com.pondit.supplychainprocess.entity.PaymentMethod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentMethodRepo extends JpaRepository<PaymentMethod,Long> {
}
