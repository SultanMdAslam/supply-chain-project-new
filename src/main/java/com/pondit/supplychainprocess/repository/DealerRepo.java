package com.pondit.supplychainprocess.repository;

import com.pondit.supplychainprocess.entity.Dealer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DealerRepo extends JpaRepository<Dealer, Long> {
}
