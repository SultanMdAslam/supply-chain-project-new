package com.pondit.supplychainprocess.repository;

import com.pondit.supplychainprocess.entity.Liter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LiterRepo extends JpaRepository<Liter, Long> {

}
