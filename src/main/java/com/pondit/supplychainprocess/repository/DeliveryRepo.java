package com.pondit.supplychainprocess.repository;

import com.pondit.supplychainprocess.entity.Delivery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeliveryRepo  extends JpaRepository<Delivery, Long> {
}
