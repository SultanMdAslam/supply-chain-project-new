package com.pondit.supplychainprocess.repository;

import com.pondit.supplychainprocess.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepo extends JpaRepository<User,Long> {
    User findByUsername(String username);
    boolean existsByUsername(String s);

    List<User> getAllByActiveFalse();
    List<User> getAllByActiveTrue();

}
