package com.pondit.supplychainprocess.repository;

import com.pondit.supplychainprocess.entity.Card;
import com.pondit.supplychainprocess.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardRepo extends JpaRepository<Card,Long> {

    List<Card> findAllByUserAndEnableFalse(User user);
}
