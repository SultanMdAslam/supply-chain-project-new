package com.pondit.supplychainprocess.init;

import com.pondit.supplychainprocess.entity.Authority;
import com.pondit.supplychainprocess.entity.User;
import com.pondit.supplychainprocess.repository.AuthorityRepo;
import com.pondit.supplychainprocess.repository.UserRepo;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Component
public class InitialDataSecurity implements CommandLineRunner {


    private final AuthorityRepo roleRepository;
    private final UserRepo userRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    public InitialDataSecurity(AuthorityRepo roleRepository, UserRepo userRepository, BCryptPasswordEncoder passwordEncoder) {
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) throws Exception {

        Authority author1 = new Authority("admin");
        Authority author2 = new Authority("dealer");
        Authority author3 = new Authority("inventoryManager");
        Authority author4 = new Authority("accountManager");
        Authority author5 = new Authority("deliveryInCharge");


        Stream.of(author1,author2,author3,author4,author5)
                .filter(s-> ! roleRepository.existsByAuthority(s.getAuthority()))
                .map(roleRepository::save)
                .forEach(System.out::println);

        String pass =passwordEncoder.encode("12345");
       /* List<Authority> aslam = new ArrayList<>();
        aslam.add(author1);
        aslam.add(author2);

        List<Authority> aslam2 = new ArrayList<>();
        aslam2.add(author2);*/

        User users1= new User("aslam", pass , author1);
       /* User users3= new User("inventory", pass , author3);
        User users4= new User("account", pass , author4);
        User users5= new User("delivery", pass , author5);*/


        Stream.of(users1)
                .filter(s-> ! userRepository.existsByUsername(s.getUsername()))
                .map(userRepository::save)
                .forEach(System.out::println);
    }
}
