package com.pondit.supplychainprocess.controller;


import com.pondit.supplychainprocess.entity.Card;
import com.pondit.supplychainprocess.entity.PaymentMethod;
import com.pondit.supplychainprocess.entity.Requisition;
import com.pondit.supplychainprocess.entity.User;
import com.pondit.supplychainprocess.repository.*;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("requisition")
public class RequisitionController {

    private final ProductRepo productRepo;
    private final PaymentMethodRepo paymentMethodRepo;
    private final RequisitionRepo requisitionRepo;
    private final CardRepo cardRepo;
    private final UserRepo userRepo;

    public RequisitionController(ProductRepo productRepo, PaymentMethodRepo paymentMethodRepo, RequisitionRepo requisitionRepo, CardRepo cardRepo, UserRepo userRepo) {
        this.productRepo = productRepo;
        this.paymentMethodRepo = paymentMethodRepo;
        this.requisitionRepo = requisitionRepo;
        this.cardRepo = cardRepo;
        this.userRepo = userRepo;
    }

    @GetMapping("/accept/{id}")
    public String deleteStudent(@PathVariable(name = "id") long id) {
        Requisition requisition = requisitionRepo.getOne(id);
        requisition.setActive(true);
        requisitionRepo.save(requisition);
        return "redirect:/account/";
    }

    @GetMapping("/delivery/{id}")
    public String deliveryProduct(@PathVariable(name = "id") long id) {
        Requisition requisition = requisitionRepo.getOne(id);
        requisition.setEnable(true);
        requisitionRepo.save(requisition);
        return "redirect:/inventory/";
    }



}
