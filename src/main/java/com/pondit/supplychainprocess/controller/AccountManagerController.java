package com.pondit.supplychainprocess.controller;

import com.pondit.supplychainprocess.entity.PaymentMethod;
import com.pondit.supplychainprocess.entity.Product;
import com.pondit.supplychainprocess.entity.Requisition;
import com.pondit.supplychainprocess.repository.PaymentMethodRepo;
import com.pondit.supplychainprocess.repository.ProductRepo;
import com.pondit.supplychainprocess.repository.RequisitionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("account")
@Component
public class AccountManagerController {

    @Autowired
    private final ProductRepo productRepo;
    @Autowired
    private final PaymentMethodRepo paymentMethodRepo;
    @Autowired
    private final RequisitionRepo requisitionRepo;

    public AccountManagerController(ProductRepo productRepo, PaymentMethodRepo paymentMethodRepo, RequisitionRepo requisitionRepo) {
        this.productRepo = productRepo;
        this.paymentMethodRepo = paymentMethodRepo;
        this.requisitionRepo = requisitionRepo;
    }

    @GetMapping("/")
    public String getRequisitionPage(Model model){
        List<Requisition> requisitions = requisitionRepo.getAllByActiveFalseAndEnableFalse();
        model.addAttribute("requisitionList",requisitions);
        return "requisition/all";
    }

    @GetMapping("/deu-payment-list")
    public String deuPaymentList(Model model){
        List<Requisition> requisitions = requisitionRepo.getAllByActiveFalseAndEnableFalse();
        model.addAttribute("requisitionList",requisitions);
        return "requisition/all";
    }

    /*@GetMapping("/")
    public String getRequisitionPage(Model model){
        List<Product> productList = productRepo.findAll();
        List<String> product = new ArrayList<>();
        List<PaymentMethod> paymentMethodList = paymentMethodRepo.findAll();
        model.addAttribute("requisition",new Requisition());
        model.addAttribute("productList", productList);
        model.addAttribute("paymentMethodList", paymentMethodList);
        return "requisition/requisition";
    }*/
}
