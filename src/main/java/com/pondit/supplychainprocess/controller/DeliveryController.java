package com.pondit.supplychainprocess.controller;

import com.pondit.supplychainprocess.entity.Card;
import com.pondit.supplychainprocess.entity.Delivery;
import com.pondit.supplychainprocess.entity.Requisition;
import com.pondit.supplychainprocess.entity.User;
import com.pondit.supplychainprocess.repository.*;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/delivery")
public class DeliveryController {

    private final ProductRepo productRepo;
    private final PaymentMethodRepo paymentMethodRepo;
    private final RequisitionRepo requisitionRepo;
    private final CardRepo cardRepo;
    private final UserRepo userRepo;
    private final DeliveryRepo deliveryRepo;


    public DeliveryController(ProductRepo productRepo, PaymentMethodRepo paymentMethodRepo, RequisitionRepo requisitionRepo, CardRepo cardRepo, UserRepo userRepo, DeliveryRepo deliveryRepo) {
        this.productRepo = productRepo;
        this.paymentMethodRepo = paymentMethodRepo;
        this.requisitionRepo = requisitionRepo;
        this.cardRepo = cardRepo;
        this.userRepo = userRepo;
        this.deliveryRepo = deliveryRepo;
    }

    @GetMapping("/all")
    public String allDelivery(Model model){
        List<Card> cards = new ArrayList<>();
        List<Requisition> requisitions = requisitionRepo.getAllByActiveTrueAndEnableTrue();
        for (Requisition requisition : requisitions){
            List<Card> eachCards = requisition.getCard();
            for (Card card: eachCards){
                if (!card.isEnable()){
                    cards.add(card);
                }
            }
        }
        model.addAttribute("cards", cards);
        return "delivery/delivery";
    }

    @GetMapping("/delivered/{id}")
    public String delivered(@PathVariable(name = "id") long id){
        Card card = cardRepo.getOne(id);
        card.setEnable(true);
        Delivery delivery = new Delivery(card.getUser(),card.getUser().getDealer().getName(),card.getProduct().getName(),card.getQuantity());
        deliveryRepo.save(delivery);
        return "redirect:/delivery/show-pending-product";
    }
}
