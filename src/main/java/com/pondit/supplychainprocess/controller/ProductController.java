package com.pondit.supplychainprocess.controller;

import com.pondit.supplychainprocess.dto.ProductDto;
import com.pondit.supplychainprocess.entity.*;
import com.pondit.supplychainprocess.repository.*;
import com.pondit.supplychainprocess.service.ProductService;
import org.springframework.beans.BeanUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

@Controller
@RequestMapping("/product")
public class ProductController {

     private final ProductRepo  productRepo;
     private final ProductService productService;
     private final LiterRepo sizeRepo;
     private final CardRepo cardRepo;
     private final UserRepo userRepo;
     private final RequisitionRepo requisitionRepo;


    public ProductController(ProductRepo productRepo, ProductService productService, LiterRepo sizeRepo, CardRepo cardRepo, UserRepo userRepo, RequisitionRepo requisitionRepo) {
        this.productRepo = productRepo;
        this.productService = productService;
        this.sizeRepo = sizeRepo;
        this.cardRepo = cardRepo;
        this.userRepo = userRepo;
        this.requisitionRepo = requisitionRepo;
    }

    @GetMapping("/add")
     public String addProduct(Model model){
         List<Liter> liters = sizeRepo.findAll();
         model.addAttribute("liters", liters);
         model.addAttribute("product", new Product());
         return "product/add-product";
     }


     @PostMapping("/save-item")
     public String saveProduct(@ModelAttribute Product product,
                               RedirectAttributes redirectAttributes,
                               @RequestParam("productImage") MultipartFile multipartFile) throws IOException {

       /*  Item item = new Item();
         BeanUtils.copyProperties(itemDto, item);
         product.setCategory(inventoryService.getCategory(itemDto.getCategoryId()));
*/
         String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
         product.setImage(fileName);
         String s = this.saveItem(product);

         if (s != "Product Already Exist") {
             String uploadDirectory = "./ProductPhoto/" + s;//Here s= Product id return after saving the product
             Path uploadPath = Paths.get(uploadDirectory);
             if (!Files.exists(uploadPath)) {
                 Files.createDirectories(uploadPath);
             }

             try (InputStream inputStream = multipartFile.getInputStream()) {
                 Path filePath = uploadPath.resolve(fileName);
                 Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
             } catch (IOException e) {
                 throw new IOException("Fail to Save the image");
             }
             redirectAttributes.addFlashAttribute("message", "Item Save Successfully");
         } else {
             redirectAttributes.addFlashAttribute("message", s);
         }
         return "redirect:/product/add";
     }

    public String saveItem(Product product) {
        if (productRepo.findByName(product.getName()) != null) {
            return "Product Already Exist";
        } else {
            productRepo.save(product);
            return String.valueOf(productRepo.findByName(product.getName()).getProductId());
        }
    }

  /*  @GetMapping(value = "/get-item-details/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Product getProductDetails(@PathVariable("id") long itemId) {
        Product product = productRepo.getOne(itemId);
        return product;
    }*/

    @GetMapping(value = "/getProduct/{id}")
    @ResponseBody public Product getProduct(@PathVariable("id") Long id1) {
        return productRepo.findById(id1).get();
    }

    @GetMapping(value = "/card/getAll")
    @ResponseBody public Card getCard() {
        return (Card) cardRepo.findAll();
    }


    /*@GetMapping(value = "/get-product-details/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Product getDetails(@PathVariable("id") long id) {
        return productRepo.getOne(id);
    }*/

    @GetMapping(value = "/get-product-details/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ProductDto getProductDetails(@PathVariable("id") long itemId) {
        Product product = productService.getProductDetails(itemId);
        ProductDto productDto = new ProductDto();
        BeanUtils.copyProperties(product,productDto);
        productDto.setImagePath("/ProductPhoto/" + productDto.getProductId() + "/" + productDto.getImage());
        return productDto;
    }
}
