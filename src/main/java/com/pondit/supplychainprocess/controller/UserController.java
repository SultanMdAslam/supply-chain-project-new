package com.pondit.supplychainprocess.controller;


import com.pondit.supplychainprocess.entity.Authority;
import com.pondit.supplychainprocess.entity.User;
import com.pondit.supplychainprocess.repository.AuthorityRepo;
import com.pondit.supplychainprocess.repository.UserRepo;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("user")
public class UserController {

    private final AuthorityRepo authorityRepo;
    private final UserRepo userRepo;
    private final BCryptPasswordEncoder passwordEncoder;

    public UserController(AuthorityRepo authorityRepo, UserRepo userRepo, BCryptPasswordEncoder passwordEncoder) {
        this.authorityRepo = authorityRepo;
        this.userRepo = userRepo;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/add")
    public String addUser(Model model){
        List<Authority> authorities = authorityRepo.findAll();
        model.addAttribute("user", new User());
        return "user/add";
    }
    @PostMapping("/save")
    public String saveUser(@ModelAttribute User user){
        Authority authority = authorityRepo.findById((long) 2).get();
        String pass =passwordEncoder.encode(user.getPassword());
       /* Dealer dealer = new Dealer();
        user.setDealer(dealer);*/
        user.setPassword(pass);
        user.setAuthority(authority);
        userRepo.save(user);
        return "redirect:/get-login-page";
    }

}
