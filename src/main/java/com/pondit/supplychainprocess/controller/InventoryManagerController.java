package com.pondit.supplychainprocess.controller;

import com.pondit.supplychainprocess.entity.Requisition;
import com.pondit.supplychainprocess.entity.User;
import com.pondit.supplychainprocess.repository.RequisitionRepo;
import com.pondit.supplychainprocess.repository.UserRepo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("inventory")
public class InventoryManagerController {

    private final RequisitionRepo requisitionRepo;
    private final UserRepo userRepo;

    public InventoryManagerController(RequisitionRepo requisitionRepo, UserRepo userRepo) {
        this.requisitionRepo = requisitionRepo;
        this.userRepo = userRepo;
    }

    @GetMapping("/")
    public String getRequisitionPage(Model model){
        List<Requisition> requisitions = requisitionRepo.getAllByActiveTrueAndEnableFalse();
        model.addAttribute("requisitionList",requisitions);
        return "requisition/all-for-inventory";
    }

    @GetMapping("/user-deactivate")
    public String getUserDeactivatePage(Model model){
        List<User> users = userRepo.getAllByActiveFalse();
        model.addAttribute("users",users);
        return "inventory/user-deactivate";
    }

    @GetMapping("/set-activate/{id}")
    public String setUserActivatePage(@PathVariable(name = "id") long id){
        User user = userRepo.getOne(id);
        user.setActive(true);
        userRepo.save(user);
        return "redirect:/inventory/user-deactivate";
    }

    @GetMapping("/user-activate")
    public String getUserActivatePage(Model model){
        List<User> users = userRepo.getAllByActiveTrue();
        model.addAttribute("users",users);
        return "inventory/user-activate";
    }


}
