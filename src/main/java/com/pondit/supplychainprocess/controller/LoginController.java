package com.pondit.supplychainprocess.controller;


import com.pondit.supplychainprocess.dto.UserDto;
import com.pondit.supplychainprocess.entity.Authority;
import com.pondit.supplychainprocess.entity.User;
import com.pondit.supplychainprocess.service.AuthorityService;
import com.pondit.supplychainprocess.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class LoginController {
    @Autowired
    UserService userService;
    @Autowired
    AuthorityService authorityService;

    @RequestMapping("/login-success")
    public RedirectView loginSuccess() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        System.out.println(user.toString());
        return new RedirectView("/");
    }

    @GetMapping("/get-login-page")
    public String getLoginPage(Model model){
        model.addAttribute("user", new User());
        return "login/sign-in-sign-up";
    }

    @GetMapping("/get-signup-page")
    public String getSignupPage(Model model){
        UserDto userDto = new UserDto();
        model.addAttribute("userDto",userDto);

        return  "login/Signup";
    }

    @PostMapping("/register")
    public String register(@ModelAttribute UserDto userDto){
        Authority authority = authorityService.getAuthority(2L);
        /*List<Authority> authorityList = new ArrayList<>();
        authorityList.add(authority);*/

       // List<Authority> authorityList = authorityService.findAll(); //getAllAuthority
        userDto.setAuthority(authority);
        userService.save(userDto);
        return "redirect:/get-login-page";

       /* Role role = roleService.findRole();  // getSingleRole
        List<Role> roleList =new ArrayList<>();
        roleList.add(role);*/


    }

}
