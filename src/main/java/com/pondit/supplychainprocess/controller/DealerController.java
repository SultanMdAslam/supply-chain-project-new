package com.pondit.supplychainprocess.controller;

import com.pondit.supplychainprocess.dto.ProductDto;
import com.pondit.supplychainprocess.entity.*;
import com.pondit.supplychainprocess.repository.*;
import org.springframework.beans.BeanUtils;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("dealer")
public class DealerController {

    private final DealerRepo dealerRepo;
    private final UserRepo userRepo;
    private final CardRepo cardRepo;
    private final PaymentMethodRepo paymentMethodRepo;
    private final DeliveryRepo deliveryRepo;
    private final RequisitionRepo requisitionRepo;
    private final ProductRepo productRepo;

    public DealerController(DealerRepo dealerRepo, UserRepo userRepo, CardRepo cardRepo, PaymentMethodRepo paymentMethodRepo, DeliveryRepo deliveryRepo, RequisitionRepo requisitionRepo, ProductRepo productRepo) {
        this.dealerRepo = dealerRepo;
        this.userRepo = userRepo;
        this.cardRepo = cardRepo;
        this.paymentMethodRepo = paymentMethodRepo;
        this.deliveryRepo = deliveryRepo;
        this.requisitionRepo = requisitionRepo;
        this.productRepo = productRepo;
    }

    @GetMapping("/")
    public String firstShow(){
        return "redirect:/dealer/add-to-cart";
    }

    @GetMapping("/requisition")
    public String getRequisitionPage(Model model){
        long totalPrice = 0;
        String currentUserName = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
        }
        User user = userRepo.findByUsername(currentUserName);
        List<Card> cards = cardRepo.findAllByUserAndEnableFalse(user);
        for (Card card: cards){
            totalPrice = totalPrice + ((card.getProduct().getPrice())*(card.getQuantity()));
        }
        List<PaymentMethod> paymentMethodList = paymentMethodRepo.findAll();
        model.addAttribute("total_price", totalPrice);
        model.addAttribute("requisition",new Requisition());
        model.addAttribute("paymentMethodList", paymentMethodList);
        return "dealer/requisition";
    }

    @GetMapping("/cart")
    public String allCard(Model model){
        long totalPrice = 0;
        String currentUserName = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
        }
        User user = userRepo.findByUsername(currentUserName);
        List<Card> cards = cardRepo.findAllByUserAndEnableFalse(user);
        for (Card card: cards){
            totalPrice = totalPrice + ((card.getProduct().getPrice())*(card.getQuantity()));
        }
        model.addAttribute("cards", cards);
        model.addAttribute("total_price", totalPrice);
        return "dealer/cart";
    }

    @GetMapping("/add-to-cart")
    public String addToCart(Model model){
        long pay = 0, deu = 0, totalPrice = 0;
        String currentUserName = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
        }
        User user = userRepo.findByUsername(currentUserName);
        List<Requisition> requisitions = requisitionRepo.findByDealerAndFullPaymentFalse(user.getDealer());
        for (Requisition requisition : requisitions) {
            pay += requisition.getInitialAmount();
            totalPrice = requisition.getTotalPrice();
        }
        deu = totalPrice - pay;
        if (!(deu <= 0)) {
            return "redirect:/dealer/deu-payment";
        }
        else {
            List<Product> products = productRepo.findAll();
            List<ProductDto> productDtoList = new ArrayList<>();
            for (Product product: products){
                ProductDto productDto = new ProductDto();
                BeanUtils.copyProperties(product,productDto);
                productDtoList.add(productDto);
            }
            BeanUtils.copyProperties(products,productDtoList);
            model.addAttribute("card", new Card());
            model.addAttribute("productList",productDtoList);
            return "dealer/add-to-cart";
        }
    }

    @GetMapping("/save-cart")
    public String saveCard(@ModelAttribute Card card){
        String currentUserName = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
        }
        User user = userRepo.findByUsername(currentUserName);
        card.setUser(user);
        cardRepo.save(card);
        return "redirect:/dealer/add-to-cart";
    }

    @GetMapping("/show-pending-product")
    public String showPendingProduct(Model model){
        String currentUserName = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
        }
        User user = userRepo.findByUsername(currentUserName);
        List<Card> cards = cardRepo.findAllByUserAndEnableFalse(user);
        model.addAttribute("cards", cards);
        return "dealer/show-pending-product";
    }

    @GetMapping("/delivered/{id}")
    public String delivered(@PathVariable(name = "id") long id){
        Card card = cardRepo.getOne(id);
        card.setEnable(true);
        Delivery delivery = new Delivery(card.getUser(),card.getUser().getDealer().getName(),card.getProduct().getName(),card.getQuantity());
        deliveryRepo.save(delivery);
        return "redirect:/dealer/show-pending-product";
    }

    @GetMapping("/add")
    public String addDealer(Model model){
        String currentUserName = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
        }
        User user = userRepo.findByUsername(currentUserName);
        if(!(user.isActive())){
            return "wait/wait";
        }
        else if(user.getDealer() == null){
            model.addAttribute("dealer", new Dealer());
            return "dealer/add";
        }
        else {
            return "redirect:/dealer/add-to-cart";
        }
    }

    @GetMapping("/save")
    public String saveDealer(@ModelAttribute Dealer dealer){
        String currentUserName = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
        }
        User user = userRepo.findByUsername(currentUserName);
        user.setDealer(dealer);
        dealer.setUser(user);
        dealerRepo.save(dealer);
        return "redirect:/welcome";
    }

    @GetMapping("/save-requisition")
    public String saveRequisition(@ModelAttribute Requisition requisition){
        long totalPrice = 0;
        String currentUserName = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
        }
        User user = userRepo.findByUsername(currentUserName);
        requisition.setDealer(user.getDealer());
        List<Card> cards = cardRepo.findAllByUserAndEnableFalse(user);
        for (Card card : cards){
            card.setRequisition(requisition);
            totalPrice = totalPrice + ((card.getProduct().getPrice())*(card.getQuantity()));
        }
        requisition.setCard(cards);
        requisition.setTotalPrice(totalPrice);
        requisitionRepo.save(requisition);
        return "redirect:/dealer/add-to-cart/";
    }

    @GetMapping("/deu-payment")
    private String deuPayment(Model model) {
        long pay = 0, deu = 0, totalPrice = 0;
        String transactionId = null;
        String currentUserName = null;

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
        }
        User user = userRepo.findByUsername(currentUserName);
        List<Requisition> requisitions = requisitionRepo.findByDealerAndFullPaymentFalse(user.getDealer());
        for (Requisition requisition : requisitions) {
            pay += requisition.getInitialAmount();
            transactionId = requisition.getTransactionId();
            totalPrice = requisition.getTotalPrice();
        }
        deu = totalPrice - pay;

            model.addAttribute("deu", deu);
            model.addAttribute("transaction", transactionId);
            model.addAttribute("total", totalPrice);
            model.addAttribute("requisition", new Requisition());
            model.addAttribute("paymentMethodList", paymentMethodRepo.findAll());
            return "dealer/deu-payment";

    }

        @GetMapping("/save-deu-payment")
        private String saveDeuPayment(@ModelAttribute Requisition newRequisition){
            long pay=0, totalPrice = 0;
            String transactionId = null;
            String currentUserName = null;

            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                currentUserName = authentication.getName();
            }
            User user = userRepo.findByUsername(currentUserName);
            List<Requisition> requisitions = requisitionRepo.findByDealerAndFullPaymentFalse(user.getDealer());
            for (Requisition requisition : requisitions){
                pay += requisition.getInitialAmount();
                transactionId = requisition.getTransactionId();
                totalPrice = requisition.getTotalPrice();
            }
            pay += newRequisition.getInitialAmount();
            if(pay >= totalPrice ){
                for (Requisition requisition : requisitions){
                    requisition.setFullPayment(true);
                }
                newRequisition.setFullPayment(true);
            }
            newRequisition.setTransactionId(transactionId);
            newRequisition.setTotalPrice(totalPrice);
            newRequisition.setEnable(true);
            newRequisition.setActive(true);
            requisitionRepo.save(newRequisition);

                return "redirect:/dealer/add-to-cart";
            }

    @GetMapping(value = "/get-item-details/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Product getDetails(@PathVariable("id") long id) {
        return productRepo.getOne(id);
    }


}

