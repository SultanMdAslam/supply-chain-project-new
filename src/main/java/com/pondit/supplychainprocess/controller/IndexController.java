package com.pondit.supplychainprocess.controller;

import com.pondit.supplychainprocess.entity.User;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
    @RequestMapping("/")
    public String addDealer(){
        String currentUserName = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
        }
        switch (currentUserName) {
            case "inventory":
                return "redirect:/inventory/";
            case "account":
                return "redirect:/account/";
            case "delivery":
                return "redirect:/delivery/";
            default:
                return "redirect:/dealer/add";
        }

    }
    @RequestMapping("/welcome")
    public String welcome(Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String currentUserName = authentication.getName();
            model.addAttribute("userName", currentUserName);
        }
        model.addAttribute("title","welcome");
        return "welcome";
    }
}
